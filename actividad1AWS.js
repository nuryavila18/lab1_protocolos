var http = require('http');
var fs = require('fs');

const port = 3000;

const server = http.createServer(function (req, res) {
    //Open a file on the server and return its content:
    fs.readFile('index.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    return res.end();
  });
});

server.listen(port, () => {
    console.log(`Server running at port: ${port}`);
  });

